import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PupsService {

    constructor(private http : HttpClient) { }

    savePups( data:any){
      return this.http.post<any>('http://localhost:8000/pups/save',data);
    }

    editPups( data:any){
      return this.http.put<any>(`http://localhost:8000/pups/${data.id}`,data);
    }
  
    
    getAll(){
      return this.http.get<any>('http://localhost:8000/pups/getAll');
    }

    getAllNoClass(){
      return this.http.get<any>('http://localhost:8000/pups/getAllNoClass');
    }

    getAllSick(){
      return this.http.get<any>('http://localhost:8000/pups/getAllSick');
    }

    getById(id:any){
      return this.http.get<any>(`http://localhost:8000/pups/getById/${id}`);
    }

    delete(data:any){
      return this.http.delete<any>(`http://localhost:8000/pups/${data.id}/${data.type}`);
    }
    

  
}
