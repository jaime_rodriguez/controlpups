import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuarantineService {

  constructor(private http : HttpClient) { }

  save( data:any){
    return this.http.post<any>('http://localhost:8000/quarantine/save',data);
  }

  edit( data:any){
    return this.http.put<any>(`http://localhost:8000/quarantine/${data.id}`,data);
  }
  
  getAll(){
    return this.http.get<any>('http://localhost:8000/quarantine/getAll');
  }

  getAllTreatments(){
    return this.http.get<any>('http://localhost:8000/quarantine/getAllTreatments');
  }

  getAllDiets(){
    return this.http.get<any>('http://localhost:8000/quarantine/getAllDiets');
  }

  getAllCorrals(){
    return this.http.get<any>('http://localhost:8000/quarantine/getAllCorrals');
  }
  
  getById(id:any){
    return this.http.get<any>(`http://localhost:8000/quarantine/getById/${id}`);
  }

  delete(data:any){
    return this.http.delete<any>(`http://localhost:8000/quarantine/${data.id}/${data.type}`);
  }
}
