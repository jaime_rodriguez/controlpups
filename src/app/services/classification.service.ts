import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClassificationService {

  constructor(private http : HttpClient) { }

  save( data:any){
    return this.http.post<any>('http://localhost:8000/classification/save',data);
  }

  edit( data:any){
    return this.http.put<any>(`http://localhost:8000/classification/${data.id}`,data);
  }

  getAll(){
    return this.http.get<any>('http://localhost:8000/classification/getAll');
  }

  getAllClass(type:number){
    return this.http.get<any>(`http://localhost:8000/classification/getAllClass/${type}`);
  }

  getById(id:any){
    return this.http.get<any>(`http://localhost:8000/classification/getById/${id}`);
  }

  delete(data:any){
    return this.http.delete<any>(`http://localhost:8000/classification/${data.id}/${data.type}`);
  }
}
