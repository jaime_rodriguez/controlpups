import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SensorsService {

  constructor(private http : HttpClient) { }

  saveSensors( data:any){
    return this.http.post<any>('http://localhost:8000/sensors/save',data);
  }

  editSensors( data:any){
    return this.http.put<any>(`http://localhost:8000/sensors/${data.id}`,data);
  }
  
  getAll(){
    return this.http.get<any>('http://localhost:8000/sensors/getAll');
  }
  
  getById(id:any){
    return this.http.get<any>(`http://localhost:8000/sensors/getById/${id}`);
  }

  delete(data:any){
    return this.http.delete<any>(`http://localhost:8000/sensors/${data.id}/${data.type}`);
  }
}
