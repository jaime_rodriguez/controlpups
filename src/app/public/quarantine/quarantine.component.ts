import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuarantineService } from 'src/app/services/quarantine.service';

@Component({
  selector: 'app-quarantine',
  templateUrl: './quarantine.component.html',
  styleUrls: ['./quarantine.component.scss']
})
export class QuarantineComponent implements OnInit {

  quarantinesData: any[] = [];

  constructor(private quarantineService: QuarantineService,
    private router: Router) { }

  ngOnInit(): void {
    this.getQuarantine();
  }

  getQuarantine() {
      this.quarantineService.getAll().subscribe(resp => {
          this.quarantinesData = resp.data
      });
  }

  edit(quiarantine: any) {
    this.router.navigate(['/registerquarantine/' + quiarantine.id]);
}

delete(pup: any,type: any) {
    const data: any = {
        id:pup.id,
        type:type
    }

    this.quarantineService.delete(data).subscribe(resp => {
        if(resp.success){
            alert(resp.message);
            this.getQuarantine();
        }else{
            alert(resp.message);
        }
        
    });
}

}
