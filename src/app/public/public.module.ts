import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { QuarantineComponent } from './quarantine/quarantine.component';
import { ClassificationComponent } from './classification/classification.component';
import { PupsComponent } from './pups/pups.component';
import { RegisterPupsComponent } from './register-pups/register-pups.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTableModule} from '@angular/material/table';
import { SensorsComponent } from './sensors/sensors.component';
import { RegisterSensorsComponent } from './register-sensors/register-sensors.component';
import { ClassPupsComponent } from './class-pups/class-pups.component';
import { FormsModule } from '@angular/forms';
import { RegisterQuarantineComponent } from './register-quarantine/register-quarantine.component';



@NgModule({
  declarations: [
    PublicComponent,
    HomeComponent,
    LoginComponent,
    QuarantineComponent,
    ClassificationComponent,
    PupsComponent,
    RegisterPupsComponent,
    SensorsComponent,
    RegisterSensorsComponent,
    ClassPupsComponent,
    RegisterQuarantineComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    FormsModule
  ]
})
export class PublicModule { }
