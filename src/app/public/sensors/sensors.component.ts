import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SensorsService } from 'src/app/services/sensors.service';

@Component({
    selector: 'app-sensors',
    templateUrl: './sensors.component.html',
    styleUrls: ['./sensors.component.scss']
})
export class SensorsComponent implements OnInit {

    sensorsData: any[] = [];

    constructor(private sensorsService: SensorsService,
        private router: Router) { }

    ngOnInit(): void {
        this.getSensors();
    }

    getSensors() {
        this.sensorsService.getAll().subscribe(resp => {
            this.sensorsData = resp.data
        });
    }

    edit(pup: any) {
        this.router.navigate(['/registersensors/' + pup.id]);
    }

    delete(pup: any,type: any) {
        const data: any = {
            id:pup.id,
            type:type
        }

        this.sensorsService.delete(data).subscribe(resp => {
            if(resp.success){
                alert(resp.message);
                this.getSensors();
            }else{
                alert(resp.message);
            }
            
        });
    }

}
