import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassPupsComponent } from './class-pups.component';

describe('ClassPupsComponent', () => {
  let component: ClassPupsComponent;
  let fixture: ComponentFixture<ClassPupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassPupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassPupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
