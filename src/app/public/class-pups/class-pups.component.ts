import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassificationService } from 'src/app/services/classification.service';
import { PupsService } from 'src/app/services/pups.service';

@Component({
    selector: 'app-class-pups',
    templateUrl: './class-pups.component.html',
    styleUrls: ['./class-pups.component.scss']
})
export class ClassPupsComponent implements OnInit {
    
    form: FormGroup;
    pups: any[] = [];
    pupsClass: any;
    classPup: string;
    classResult: number;

    constructor(private fb: FormBuilder,
        private pupsService: PupsService,
        private classificationService: ClassificationService,
        private route: ActivatedRoute,
        private router: Router,) 
    {
            this.loadDataPups();
            this.createForm();
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(res => {
            let id = parseInt(res.get('id'));

            if (id) {
                this.loadDataPup(id);
            }

        });

        
    }

    loadDataPup(id) {
        this.classificationService.getById(id).subscribe(resp => {
            this.pupsClass = resp.data[0];
            this.createForm();
            this.classPup= this.pupsClass.className;
            let arr: any[] =[{id:this.pupsClass.idpup,name:this.pupsClass.name}] ;
            this.pups= arr;
            
        });
    }
    
    loadDataPups() {
        this.pupsService.getAllNoClass().subscribe(resp => {
            this.pups = resp.data;
            // this.createForm();
        });
    }

    createForm() {
        this.form = this.fb.group({
            idpup: [this.pupsClass ? this.pupsClass.idpup : '', Validators.required],
            weight: [this.pupsClass ? this.pupsClass.weight : '', Validators.required],
            muscle: [this.pupsClass ? this.pupsClass.muscle : '', Validators.required],
            marbling: [this.pupsClass ? this.pupsClass.marbling : '', Validators.required],
            idclass: [this.pupsClass ? this.pupsClass.idclass : ''],
            id: this.pupsClass ? this.pupsClass.id : ''
        });
    }

    onSubmit(){
        if (this.form.valid) {
            
            const formData = this.form.getRawValue();
            let id = formData.id;
            let saved = null;

            const data:any = {
                idpup: formData.idpup,
                weight:formData.weight,
                muscle:formData.muscle,
                marbling:formData.marbling,
                idclass:this.classResult,
                id: id
            }

            if (id) { //edit
                saved = this.classificationService.edit(data);
            } else {
                saved = this.classificationService.save(data);
            }

            saved.subscribe(res => {
                if (res.success) {
                    this.form.reset();
                    alert(res.message);
                    this.router.navigate(['/classification']);
                } else {
                    alert(res.message);
                }
            })
        }else{
            alert("Complete el todos los campos del formuario");
        }
    }

    results(){
        
        if (this.form.valid) {
            const formData = this.form.getRawValue();
            let weight= formData.weight;
            let muscle= formData.muscle;
            let marbling= formData.marbling;

            if((weight >=15 && weight <= 25) && (muscle >= 3 && muscle <= 5) && (marbling <=2)){
                console.log("marmoleo 1");
                this.classResult=2;
                this.classPup= "Grasa Tipo 1";
            }else{
                this.classResult=3;
                this.classPup= "Grasa Tipo 2";
            }

        } else {
            alert("Complete el todos los campos del formuario");
        }
    }

}
