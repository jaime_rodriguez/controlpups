import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterQuarantineComponent } from './register-quarantine.component';

describe('RegisterQuarantineComponent', () => {
  let component: RegisterQuarantineComponent;
  let fixture: ComponentFixture<RegisterQuarantineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterQuarantineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterQuarantineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
