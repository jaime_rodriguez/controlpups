import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PupsService } from 'src/app/services/pups.service';
import { QuarantineService } from 'src/app/services/quarantine.service';

@Component({
    selector: 'app-register-quarantine',
    templateUrl: './register-quarantine.component.html',
    styleUrls: ['./register-quarantine.component.scss']
})
export class RegisterQuarantineComponent implements OnInit {


    form: FormGroup;
    pups: any[] = [];
    diets: any[] = [];
    treatments: any[] = [];
    corrals: any[] = [];
    pupsQuarantine: any;
    heart_rate: string;
    blood_pressure: string;
    b_frequency: string;
    temperature: string;

    constructor(
        private fb: FormBuilder,
        private pupsService: PupsService,
        private quarantineService: QuarantineService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.loadDataPups();
        this.loadDataDiets();
        this.loadDataTreatments();
        this.loadDataCorrals();
        this.createForm();
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(res => {
            let id = parseInt(res.get('id'));

            if (id) {
                this.loadDataSensor(id);
            }

        });
    }

    loadDataSensor(id) {
        this.quarantineService.getById(id).subscribe(resp => {
            this.pupsQuarantine = resp.data[0];
            this.createForm();
            this.SelectedRow();
        });
    }

    loadDataPups() {
        this.pupsService.getAllSick().subscribe(resp => {
            this.pups = resp.data;
        });
    }

    loadDataTreatments() {
        this.quarantineService.getAllTreatments().subscribe(resp => {
            this.treatments = resp.data;
        });
    }
    loadDataDiets() {
        this.quarantineService.getAllDiets().subscribe(resp => {
            this.diets = resp.data;
        });
    }
    loadDataCorrals() {
        this.quarantineService.getAllCorrals().subscribe(resp => {
            this.corrals = resp.data;
        });
    }

    createForm() {
        this.form = this.fb.group({
            idpup: [this.pupsQuarantine ? this.pupsQuarantine.idpup : '', Validators.required],
            iddiet: [this.pupsQuarantine ? this.pupsQuarantine.iddiet : '', Validators.required],
            idtreatment: [this.pupsQuarantine ? this.pupsQuarantine.idtreatment : '', Validators.required],
            idcorral: [this.pupsQuarantine ? this.pupsQuarantine.idcorral : '', Validators.required],
            id: this.pupsQuarantine ? this.pupsQuarantine.id : ''
        });
    }

    onSubmit() {
        if (this.form.valid) {
            const formData = this.form.getRawValue();
            let id = formData.id;
            let saved = null;

            const data: any = {
                idpup: formData.idpup,
                iddiet: formData.iddiet,
                idtreatment: formData.idtreatment,
                idcorral: formData.idcorral,
                id: id
            }

            if (id) { //edit
                saved = this.quarantineService.edit(data);
            } else {
                saved = this.quarantineService.save(data);
            }

            saved.subscribe(res => {
                if (res.success) {
                    this.form.reset();
                    alert(res.message);
                    this.router.navigate(['/quarantine']);
                } else {
                    alert(res.message);
                }
            })
        } else {
            alert("Complete el todos los campos del formuario");
        }
    }

    SelectedRow(){
        const formData = this.form.getRawValue();
        if(formData.idpup != ""){
            this.pups.forEach((value) => {
                if(value['idpup'] == formData.idpup){
                    this.heart_rate=value['heart_rate'];
                    this.b_frequency=value['b_frequency'];
                    this.blood_pressure=value['blood_pressure'];
                    this.temperature=value['temperature'];
                }
                console.log(value['b_frequency']);
            })
        }else{
            this.heart_rate="";
            this.b_frequency="";
            this.blood_pressure="";
            this.temperature="";
        }
        
    }


}
