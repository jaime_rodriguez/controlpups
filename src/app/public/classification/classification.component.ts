import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClassificationService } from 'src/app/services/classification.service';

@Component({
    selector: 'app-classification',
    templateUrl: './classification.component.html',
    styleUrls: ['./classification.component.scss']
})
export class ClassificationComponent implements OnInit {

    classData: any[] = [];

    constructor(
        private classificationService: ClassificationService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.getClassification();
    }

    getClassification() {
        this.classificationService.getAll().subscribe(resp => {
            this.classData = resp.data
        });
    }

    edit(clas: any) {
        this.router.navigate(['/classpups/' + clas.id]);
    }

    delete(clas: any, type: any) {
        const data: any = {
            id: clas.id,
            type: type
        }

        this.classificationService.delete(data).subscribe(resp => {
            if (resp.success) {
                alert(resp.message);
                this.getClassification();
            } else {
                alert(resp.message);
            }

        });
    }

}
