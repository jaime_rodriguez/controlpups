import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProvidersService } from 'src/app/services/providers.service';
import { PupsService } from 'src/app/services/pups.service';

@Component({
    selector: 'app-register-pups',
    templateUrl: './register-pups.component.html',
    styleUrls: ['./register-pups.component.scss']
})
export class RegisterPupsComponent implements OnInit {
    title = "Registro de Crias";
    form: FormGroup;
    idPups: number;
    pups: any;
    providersData: any[] = [];

    constructor(
        private fb: FormBuilder,
        private pupsService: PupsService,
        private route: ActivatedRoute,
        private router: Router,
        private providersService: ProvidersService
        // private messageService: MessagesServices
    ) {
        this.loadProviders();
        this.createForm();
    }

    ngOnInit(): void {

        this.route.paramMap.subscribe(res => {
            let id = parseInt(res.get('id'));

            if (id) {
                this.loadDataPup(id);
            }

        });
    }

    loadProviders(){
        this.providersService.getAll().subscribe(resp => {
            this.providersData = resp.data;
            console.log(this.providersData);
        });
    }

    loadDataPup(id) {
        this.pupsService.getById(id).subscribe(resp => {
            this.pups = resp.data[0];
            this.createForm();
        });
    }

    createForm() {
        this.form = this.fb.group({
            name: [this.pups ? this.pups.name : '', Validators.required],
            description: [this.pups ? this.pups.description : '', Validators.required],
            supplier: [this.pups ? this.pups.supplier : '', Validators.required],
            weight: [this.pups ? this.pups.weight : '', Validators.required],
            cost: [this.pups ? this.pups.cost : '', Validators.required],
            id: this.pups ? this.pups.id : ''
        });
    }

    onSubmit() {

        if (this.form.valid) {

            const formData = this.form.getRawValue();
            let id = formData.id;
            let saved = null;

            const data: any = {
                id : formData.id,
                name: formData.name,
                description: formData.description,
                supplier: formData.supplier,
                weight: formData.weight,
                cost: formData.cost
            }

            if (id) { //edit
                saved = this.pupsService.editPups(data);
            } else {
                saved = this.pupsService.savePups(data);
            }

            saved.subscribe(res => {
                if (res.success) {
                    this.form.reset();
                    alert(res.message);
                    this.router.navigate(['/pups']);
                } else {
                    alert(res.message);
                }
            })

        } else {
            alert("Complete el todos los campos del formuario");
        }
    }

}
