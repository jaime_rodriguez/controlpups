import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassificationService } from 'src/app/services/classification.service';
import { SensorsService } from 'src/app/services/sensors.service';

@Component({
    selector: 'app-register-sensors',
    templateUrl: './register-sensors.component.html',
    styleUrls: ['./register-sensors.component.scss']
})
export class RegisterSensorsComponent implements OnInit {

    form: FormGroup;
    pups: any[] = [];
    pupsSensors: any;

    constructor(
        private fb: FormBuilder,
        private sensorsService: SensorsService,
        private classificationService: ClassificationService,
        private route: ActivatedRoute,
        private router: Router,
    ) { 
        this.loadDataPups();
        this.createForm();
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(res => {
            let id = parseInt(res.get('id'));

            if (id) {
                this.loadDataSensor(id);
            }

        });
    }

    loadDataSensor(id) {
        this.sensorsService.getById(id).subscribe(resp => {
            this.pupsSensors = resp.data[0];
            this.createForm();
            let arr: any[] =[{idpup:this.pupsSensors.idpup,name:this.pupsSensors.name, sel:true}] ;
            this.pups= arr;
            
            
        });
    }

    loadDataPups() {
        this.classificationService.getAllClass(3).subscribe(resp => {
            this.pups = resp.data;
            // this.createForm();
        });
    }

    createForm() {
        this.form = this.fb.group({
            idpup: [this.pupsSensors ? this.pupsSensors.idpup : '', Validators.required],
            heart_rate: [this.pupsSensors ? this.pupsSensors.heart_rate : '', Validators.required],
            blood_pressure: [this.pupsSensors ? this.pupsSensors.blood_pressure : '', Validators.required],
            b_frequency: [this.pupsSensors ? this.pupsSensors.b_frequency : '', Validators.required],
            temperature: [this.pupsSensors ? this.pupsSensors.temperature : '', Validators.required],
            id: this.pupsSensors ? this.pupsSensors.id : ''
        });
    }

    onSubmit(){
        if (this.form.valid) {
            
            const formData = this.form.getRawValue();
            let id = formData.id;
            let saved = null;

            const data:any = {
                idpup: formData.idpup,
                heart_rate:formData.heart_rate,
                blood_pressure:formData.blood_pressure,
                b_frequency:formData.b_frequency,
                temperature:formData.temperature,
                id: id
            }

            if (id) { //edit
                saved = this.sensorsService.editSensors(data);
            } else {
                saved = this.sensorsService.saveSensors(data);
            }

            saved.subscribe(res => {
                if (res.success) {
                    this.form.reset();
                    alert(res.message);
                    this.router.navigate(['/sensors']);
                } else {
                    alert(res.message);
                }
            })
        }else{
            alert("Complete el todos los campos del formuario");
        }
    }

}
