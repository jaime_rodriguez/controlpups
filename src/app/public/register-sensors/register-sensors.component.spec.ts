import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSensorsComponent } from './register-sensors.component';

describe('RegisterSensorsComponent', () => {
  let component: RegisterSensorsComponent;
  let fixture: ComponentFixture<RegisterSensorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterSensorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSensorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
