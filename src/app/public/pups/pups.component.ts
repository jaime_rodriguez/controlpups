import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PupsService } from 'src/app/services/pups.service';

@Component({
    selector: 'app-pups',
    templateUrl: './pups.component.html',
    styleUrls: ['./pups.component.scss']
})
export class PupsComponent implements OnInit {

    pupsData: any[] = [];

    constructor(
        private pupsService: PupsService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.getPups();
    }

    getPups() {
        this.pupsService.getAll().subscribe(resp => {
            this.pupsData = resp.data
        });
    }

    edit(pup: any) {
        this.router.navigate(['/registerpups/' + pup.id]);
    }

    delete(pup: any,type: any) {
        const data: any = {
            id:pup.id,
            type:type
        }

        this.pupsService.delete(data).subscribe(resp => {
            if(resp.success){
                alert(resp.message);
                this.getPups();
            }else{
                alert(resp.message);
            }
            
        });
    }



}
