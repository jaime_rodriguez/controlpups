import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClassPupsComponent } from './public/class-pups/class-pups.component';
import { ClassificationComponent } from './public/classification/classification.component';
import { HomeComponent } from './public/home/home.component';
import { LoginComponent } from './public/login/login.component';
import { PublicComponent } from './public/public.component';
import { PupsComponent } from './public/pups/pups.component';
import { QuarantineComponent } from './public/quarantine/quarantine.component';
import { RegisterPupsComponent } from './public/register-pups/register-pups.component';
import { RegisterQuarantineComponent } from './public/register-quarantine/register-quarantine.component';
import { RegisterSensorsComponent } from './public/register-sensors/register-sensors.component';
import { SensorsComponent } from './public/sensors/sensors.component';
import { SecureComponent } from './secure/secure.component';

const routes: Routes = [
  {
    path: '',
    component:PublicComponent,
    children:[
      {path: '', component:HomeComponent},
      {path: 'login', component:LoginComponent},
      {path: 'pups', component:PupsComponent},
      {path: 'registerpups', component:RegisterPupsComponent},
      {path: 'registerpups/:id', component:RegisterPupsComponent},
      {path: 'quarantine', component:QuarantineComponent},
      {path: 'registerquarantine', component:RegisterQuarantineComponent},
      {path: 'registerquarantine/:id', component:RegisterQuarantineComponent},
      {path: 'classification', component:ClassificationComponent},
      {path: 'classpups', component:ClassPupsComponent},
      {path: 'classpups/:id', component:ClassPupsComponent},
      {path: 'sensors', component:SensorsComponent},
      {path: 'registersensors', component:RegisterSensorsComponent},
      {path: 'registersensors/:id', component:RegisterSensorsComponent},
    ]
  },
  {path: 'secure', component: SecureComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
